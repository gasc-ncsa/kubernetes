# Help

Students should speak to the camp instructors about questions or problems they encounter during the event. For any other questions, please contact the organizers at [✉️ gasc-organizers@lists.ncsa.illinois.edu](mailto:gasc-organizers@lists.ncsa.illinois.edu?&subject=%5BGASC%5D%20Help%20Request).
