# GASC website

There is a [🌐 GASC "landing page" website](https://gasc.cosmology.illinois.edu) deployed for convenience of the instructors and camp coordinators at `https://gasc.cosmology.illinois.edu`. It contains links to this documentation and the other services as well as information about the camp. [It is a simple HTML page](https://gitlab.com/gasc-ncsa/kubernetes/-/tree/master/src/landing-page/src) that is easy to update with content.

# Email list

There is an email list `gasc-organizers@lists.ncsa.illinois.edu` available for use by the organizers. The list can be configured at https://lists.ncsa.illinois.edu/lists/info/gasc-organizers to allow anyone to send messages to that email address and have select organizers receive the emails. This is currently used on the GASC website (described above) as a way to contact organizers:

```html
<div>
    Questions? <a href="mailto:gasc-organizers@lists.ncsa.illinois.edu?subject=[GASC]%20Question/Comment">Contact us here</a>
</div>
```

# Computing infrastructure

In 2024 we used a dedicated Kubernetes cluster running on Jetstream2 via the NSF ACCESS program. The git repo driving the deployment is at [https://gitlab.com/gasc-ncsa/kubernetes](https://gitlab.com/gasc-ncsa/kubernetes), with all GASC-related deployments controlled by [the "root app"](https://gitlab.com/gasc-ncsa/kubernetes/-/tree/master/apps/root). The cluster was [bootstrapped using Terraform and Rancher](https://git.ncsa.illinois.edu/decentci/jetstream2).
