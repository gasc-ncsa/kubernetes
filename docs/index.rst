Girls Astronomy Summer Camp Computing Services
==============================================

This is the documentation for the computing services built and operated by `NCSA <https://www.ncsa.illinois.edu/>`_ for the `UIUC Girls Astronomy Summer Camp <https://gasc.cosmology.illinois.edu>`_. 

Help
------------

Students should speak to the camp instructors about questions or problems they encounter during the event. For any other questions, please contact the organizers at [✉️ gasc-organizers@lists.ncsa.illinois.edu](mailto:gasc-organizers@lists.ncsa.illinois.edu?&subject=%5BGASC%5D%20Help%20Request).

.. toctree::
   :maxdepth: 1
   :caption: JupyterHub
   :glob:

   jupyterhub

.. toctree::
   :maxdepth: 2
   :glob:
   :caption: Infrastructure

   infrastructure

.. toctree::
   :maxdepth: 1
   :caption: Reports
   :glob:

   reports/*
