# 2023 Girls Astronomy Summer Camp Report

**T. Andrew Manning**, *Research Scientist*, National Center for Supercomputing Applications

### Summary

I supported another successful [UIUC Girls Astronomy Summer Camp](https://gasc.cosmology.illinois.edu), which was held June 26-27, 2023. This year I again deployed a dedicated JupyterHub service that allowed the students to run code in Jupyter notebooks independently on their own JupyterLab servers during the tutorial session. My points of contact were the camp instructors Maggie Verrico and Aidan Berres.

### JupyterHub

The JupyterHub deployment was configured in the same way as last year. See the previous year's report.

### Kubernetes cluster

We again used our existing SPT-3G cluster for the sake of efficiency and minimizing cost.

### Identity and access management

Because they are in high school, the students needed local accounts that do not rely on an email address or a third-party service. I used the existing SPT-3G Keycloak server for this purpose but used a new "realm" dedicated to the event. Camp instructors were given the role of "realm admins" so that they could subsequently add or modify user accounts on their own via the Keycloak web interface. User account provisioning was streamlined by again using a shared online spreadsheet (Google Docs): camp instructors entered instructor and student names in the spreadsheet using a web browser. The improvement over last year was that there is now a deployed service that periodically checks this spreadsheet to automatically provision new accounts via the Keycloak API based on the camper info that has been added, sending alert messages about new accounts via Matrix.
