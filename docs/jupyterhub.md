# JupyterHub

## Create account

GASC uses local accounts that only exist temporarily on our servers. Visit the [🌐 GASC JupyterHub signup page](https://jupyter.gasc.cosmology.illinois.edu/hub/signup) and register a new account with a username and password of your choice. **After an admin authorizes your new account**, proceed [🌐 to login](https://jupyter.gasc.cosmology.illinois.edu/hub/login).

## Launch your JupyterLab server

Upon login, your server will automatically spawn and you will be connected to it. You can also [🌐 visit your home page](https://jupyter.gasc.cosmology.illinois.edu/hub/home) where you can click buttons to either connect to your server or stop it.

Each JupyterLab server is allocated a small amount of private persistent storage. All JupyterLab servers share a much larger capacity storage mounted to `/home/jovyan/shared`. This folder is readable and writable by all users. It is recommended that participants use their own subfolder (e.g. `/home/jovyan/shared/vera.rubin`) for their notebook and data files.

## Data initialization

When a user launches thir server for the first time, typically upon first login, a script will copy the contents of `/shared/tutorial` into a personal folder at `/shared/$USERNAME/tutorial`. This provides instructors a convenient way for each student to receive an independent copy of the tutorial files so they can immediately start tinkering. This initialization only occurs when a server starts **and** only if a `/shared/$USERNAME` folder does not exist, to avoid overwriting modified data.

## How to log out

From JupyterLab, select **File > Log Out**

## Jupyter admins

If you are an instructor, you are a Jupyter admin who can [control the student JupyterLab servers](https://jupyter.gasc.cosmology.illinois.edu/hub/admin) as well as [manage the user accounts](https://jupyter.gasc.cosmology.illinois.edu/hub/authorize) (see [the docs for NativeAuthenticator](https://native-authenticator.readthedocs.io/en/stable/quickstart.html#) for more detail).

## Computing resources

Each JupyterLab server is guaranteed 0.5 CPU and 1GB memory, with a usage limit of 1 CPU and 2GB memory. There are three worker nodes in the Kubernetes cluster hosting these servers, each with 8 CPU and 16GB memory.

Each JupyterLab server has 1GB of private persistent storage under `/home/jovyan`, and a larger volume of shared storage under `/home/jovyan/shared`. Every user has write access to all files under the shared folder, so care should be taken not to accidentally overwrite or delete files of the other participants.

The JupyterLab server runs [a container image built from this Dockerfile](https://gitlab.com/gasc-ncsa/kubernetes/-/blob/master/src/jupyter/lab/Dockerfile), where additional astronomy-related packages are installed on top of the official `scipy-notebook` [JupyterLab server image](https://jupyter-docker-stacks.readthedocs.io/en/latest/).
