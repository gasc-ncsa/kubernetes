Girls Astronomy Summer Camp
===========================================

This repository holds the Kubernetes deployment configuration and supporting files for the Girls Astronomy Summer Camp web services.

See https://gasc.cosmology.illinois.edu/docs for documentation (source: [`/docs`](./docs/)).
