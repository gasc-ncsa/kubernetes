# Jupyter images

There are two custom images used in the JupyterHub deployment:

## Hub

The Hub image is customized to install the `keycloakauthenticator` package required for integration with our Keycloak IAM system.

## Lab

The JupyterLab server image starts from the official `scipy-notebook` image and installs additional packages required for the GASC tutorial notebooks.
